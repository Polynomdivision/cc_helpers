# CC Helpers
A header-only library of functions which I frequently use and am tired of reimplementing.

## Headers
### vectors.hpp
- `map(...)`: Maps all elements to another element (see JavaScript's `array.map(...)`)
- `filter(...)`: Filters out elements for which the function does not return true (see JavaScript's `array.filter(...)`)
- `reduce(...)`: Reduces all elements of a vector onto one single element (see JavaScript's `array.reduce(...)`)
- `slice(...)`: Returns a slice of a vector's data (see Python's `array[a:b]`)
- `print_vector(...)`: For debuggin using cout; prints the array contents
### strings.hpp
- `pad(...)`: Pads a string to a given length using a provided string
- `split(...)`: Splits a string on every occurence of a certain string

## Tests
Tests are compiled with Catch.

To build tests run `make tests` and run the executables in `build/`.

## License
See `LICENSE`



