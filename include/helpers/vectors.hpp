#pragma once
#include <vector>
#include <iostream>
#include <functional>

using namespace std;

namespace helpers {
  /*
   * Returns all elements whoose index fulfills the requirement that the index i
   * is element of the interval [from; to].
  **/
  template<typename T>
  vector<T*> slice(const vector<T>& arr, int from, int to) {
    // Bound checking
    if (from < 0 || to >= arr.size())
      return {};

    vector<T*> tmp;
    for (int i = from; i <= to; i++)
      tmp.push_back((T*) arr.data() + i);

    return tmp;
  }

  /*
   * Prints all elements from a vector to stdout.
  **/
  template<typename T>
  void print_vector(const vector<T>& arr) {
    for (int i = 0; i < arr.size(); i++)
      cout << "[" << i << "] " << arr[i] << endl;
  }

  /*
   * Maps all elements from @arr the output of @f.
  **/
  template<typename U, typename V>
  vector<U> map(const vector<V>& arr, function<U(V)> f) {
    vector<U> tmp;

    for (auto el = arr.begin(); el != arr.end(); el++)
      tmp.push_back(f(*el));

    return tmp;
  }

  /*
   * The returned vector will only contain elements from @arr for which @f
   * returns true.
  **/
  template<typename T>
  vector<T*> filter(const vector<T>& arr, function<bool(T)> f) {
    vector<T*> tmp;
    for (int i = 0; i < arr.size(); i++)
      if (f(arr[i]))
        tmp.push_back((T*) arr.data() + i);

    return tmp;
  }

  /*
   * Reduces @arr to a single value. For that, f is called for every element
   * in @arr.
   *
   * Arguments:
   *   @arr: The array that is to reduce
   *   @f: The reducer.
   *       First argument: The current element
   *       Second argument: The accumulator
   *       Return: The new accumulator
   *   @start: The accumulator's initial value
  **/
  template<typename U, typename V>
  V reduce(const vector<U>& arr, function<V(U, V)> f, const V start) {
    V val = start;
    for (auto el = arr.begin(); el != arr.end(); el++)
      val = f(*el, val);

    return val;
  }
} // helpers
