#pragma once
#include <string>
#include <vector>

using namespace std;

namespace helpers {
  /*
   * Pads a string to @length using @padder.
   * TODO: It is expected that padder is just one character
   **/
  string pad(const string& str, int length, const string padder) {
    if (str.size() >= length)
      return str;

    // TODO: Not sure if this leaves @str intact...
    string tmp = str;
    for (int i = length - tmp.size(); i > 0; i -= padder.length())
      tmp += padder;

    return tmp;
  }

  /*
   * Splits @str on every occurence of @delim. Each substring
   * is then returned as a vector.
  **/
  vector<string> split(string str, string delim) {
    int last = 0;
    int i = 0;
    vector<string> tmp;

    while ((i = str.find(delim, last)) != string::npos) {
      tmp.push_back(str.substr(last, i - last));
      last = i + delim.length();
    }

    tmp.push_back(str.substr(last, str.length() - last));
  
    return tmp;
  }
} // helpers
