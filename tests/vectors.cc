#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "helpers/vectors.hpp"

TEST_CASE("Vectors are correctly mapped") {
  auto test_data = {1, 2, 3, 4, 5};
  auto expectation = vector<int>{1, 4, 9, 16, 25};

  auto square = [](const int& el) { return el * el; };
  auto result = helpers::map<int, int>(test_data, square);

  REQUIRE(result.size() == expectation.size());
  for(int i = 0; i < expectation.size(); i++)
    REQUIRE(result[i] == expectation[i]);
}

TEST_CASE("Vectors are correctly filtered") {
  const vector<int> test_data = {1, 2, 3, 4, 5, 6};
  auto expectation = vector<int>{2, 4, 6};

  auto even = [](const int& el) { return el % 2 == 0; };
  auto result = helpers::filter<int>(test_data, even);

  REQUIRE(result.size() == expectation.size());
  for(int i = 0; i < expectation.size(); i++)
    REQUIRE(*result[i] == expectation[i]);
}

TEST_CASE("Vectors are correctly reduced") {
  auto test_data = {1, -2, 3, -4, 5, -6};

  auto sum = [](const int& el, const int& reducer) { return reducer + el; };
  int result = helpers::reduce<int, int>(test_data, sum, 0);

  REQUIRE(result == -3);
}

TEST_CASE("Vectors are correctly sliced") {
  const vector<int> test_data = {1, 2, 3, 4, 5, 6};
  auto result = helpers::slice<int>(test_data, 2, 4);

  REQUIRE(result.size() == 3);
  for(int i = 0; i < result.size(); i++)
    REQUIRE(*result[i] == i + 3);
}
