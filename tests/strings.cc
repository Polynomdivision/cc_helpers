#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "helpers/strings.hpp"

TEST_CASE("Strings are correctly split...") {
  auto result = helpers::split("this is a test", " ");
  REQUIRE(result[0] == "this");
  REQUIRE(result[1] == "is");
  REQUIRE(result[2] == "a");
  REQUIRE(result[3] == "test");

  result = helpers::split("1,,2,,3,,4", ",,");
  REQUIRE(result[0] == "1");
  REQUIRE(result[1] == "2");
  REQUIRE(result[2] == "3");
  REQUIRE(result[3] == "4");
}

TEST_CASE("Strings are correctly padded") {
  REQUIRE(helpers::pad("GitHub", 8, "-") == "GitHub--");
  REQUIRE(helpers::pad("Polynom", 4, "x") == "Polynom");
}
