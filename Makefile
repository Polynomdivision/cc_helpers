build:
	mkdir build/; exit 0
build/test_strings: build include/helpers/strings.hpp tests/strings.cc
	g++ -std=c++17 -I./include/ -lstdc++ tests/strings.cc -I./third_party \
		-o build/test_strings
build/test_vectors: build include/helpers/vectors.hpp tests/vectors.cc
	g++ -std=c++17 -I./include/ -lstdc++ tests/vectors.cc -I./third_party \
		-o build/test_vectors

tests: build/test_strings build/test_vectors
